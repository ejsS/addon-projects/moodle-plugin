<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * History of ejsS simulation
 *
 * @package    mod_laejss
 * @copyright  2022 Felix J. Garcia
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(dirname(__FILE__).'/../../config.php');
require_once(__DIR__. '/constants.php');
require_once(__DIR__. '/report_lti_lib.php');

$title =  optional_param('title', '', PARAM_TEXT);
$cmid = optional_param('cm', -1, PARAM_INT);
$cminstance = optional_param('cminstance', -1, PARAM_INT);
$courseid = optional_param('course', -1, PARAM_INT);
$resourcelink = optional_param('resourcelink', '', PARAM_TEXT);
        
// Make sure they can even access this course
if (!$course = $DB->get_record('course', array('id' => $courseid))) {
    print_error('invalidcourseid');
}

require_login($course);
$contextmodule = context_module::instance($cmid);
require_capability('report/progress:view', $contextmodule);
$PAGE->set_course($course);

// get assessment definition
$fs = get_file_storage();
$file = $fs->get_file($contextmodule->id, 'mod_laejss', 'content', 0, '/', 'assessment.json');
if ($file) {
    $contents = $file->get_content();
	// clean comments and blank lines
	$contents = preg_replace('!^[ \t]*/\*.*?\*/[ \t]*[\r\n]!s', '', $contents);
	$contents = preg_replace('![ \t]*//.*[ \t]*[\r\n]!', '', $contents);
	$contents = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $contents);
	$assessment = json_decode($contents);
	// print_error($contents);
} else {
	print_error("Assessment file not found!");
}

// check content
if (!isset($assessment->{'tasks'})) {
	print_error("Assessment file error: bad format!");
}

// assessment ranks
$ranks = [];
$header = [];
$ranksum = 0;
foreach ($assessment->{'tasks'} as $task) {
	$lastmark = end($task->{'marks'});
	$ranksum = $ranksum + $lastmark->{'value'};
	$ranks[] = [ "name" => $task->{'name'}, "maxrank" => $lastmark->{'value'} ];
	$header[] = [ "maxrank" => $lastmark->{'value'}, "description" => $task->{'description'}, 
					"name" => $task->{'name'}, "not" => 0, "correct" => 0, "partial" => 0, "wrong" => 0 ];
}

// data for template
$data = [
    'title' => $title,
	'header' => $header,
	'ranks' => $ranks,
	'users' => [],
	'overview' => []
];

// use resourcelink in sql
if (!empty($resourcelink)) {
	$resourcelinkfilter = ' AND ltiresourcelink = "' . $resourcelink . '"';
} else {
	$resourcelinkfilter = "";
}

// get views and users
$viewsperuser = $DB->get_recordset_select(PLUGIN_VIEWS_TABLE_NAME, 'contextinstanceid = ' . $cmid . $resourcelinkfilter .
				' GROUP BY userid', null, 
				'userid DESC', "userid, COUNT(timestamp) AS ct");
if ($viewsperuser->valid()) {
	$complete = 0;
	$incomplete = 0;
	$overdue = 0;
	foreach ($viewsperuser as $viewperuser) {
		if(!($user = $DB->get_record(USER_TABLE_NAME, array('id'=>$viewperuser->userid))))
			continue;

		// get online status
		$sess = $DB->get_recordset_select(SESSIONS_TABLE_NAME, 'userid = ' . $viewperuser->userid . ' GROUP BY userid', null, '', "userid");
		if($sess->valid()) {
			$online = 'slsonline';  // online
		} else {
			$online = 'btn-secondary';  // offline
		}
	
		// get all actions for the user
		$interactions = mod_laejss_lti_getInteractions($viewperuser->userid, $cmid, $resourcelink);

		// check each assessment task
		$tasks = $assessment->{'tasks'};
		$donetasks = array_fill(0, count($tasks), null); // for default, pending
		$marksum = 0;
		foreach ($tasks as $taskix => $task) {
			// get last interactions period for this task
			if (!isset($task->{'events'})) {
				$lastperiod = $interactions; // no limits in period
			} else { 
				$eventstask = $task->{'events'};
				$periods = mod_laejss_lti_findInteractionsPeriod($interactions, $eventstask->{'start'}, $eventstask->{'end'}); 
				if (count($periods) > 0) {
					$lastperiodix = end($periods);
				$lastperiod = array_slice($interactions[$lastperiodix['view']], $lastperiodix['start'], $lastperiodix['end'] - $lastperiodix['start'] + 1); 
				} else {
					$lastperiod = [];
				}
			}
			
			// check task done
			if (count($lastperiod) == 0) {
				// end task assessment, the rest of task remain pending
				//break; // commented out to prevent breaking out of iteration
				continue; // added by lookang and ryan, to continue to search for start and end
			}

			// check each mark for the task	
			$markstocheck = $task->{'marks'};
			$endmark = 0;
			foreach ($markstocheck as $mark) {
				$result = mod_laejss_lti_checkState($lastperiod, $mark->{'state'}, $task->{'history'}, $user->firstname);
				$endhistory = $result['history'];
				if (!$result['found'])
					// break; // commented out to prevent breaking out of checking 
					continue; 
				$endmark = $mark->{'value'};
			}
			$marksum = $marksum + $endmark;
			$donetasks[$taskix] = [ 'mark' => $endmark, 'history' => $endhistory ];
		}

		// set template values for each assessment task
		$assstatus = [ 'color' => 'slsoverdue', 'text' => 'O', 'marksum' => $marksum, 'markmax' => $ranksum ]; // assessment status, pending
		if (!empty($donetasks) and $donetasks[count($donetasks)-1] != null)
			$assstatus = [ 'color' => 'slscomplete', 'text' => 'C', 'marksum' => $marksum, 'markmax' => $ranksum ];  // complete
		elseif (!empty($donetasks) and $donetasks[0] != null)
			$assstatus = [ 'color' => 'slspartially', 'text' => 'I', 'marksum' => $marksum, 'markmax' => $ranksum ];  // incomplete
		
		$taskstatus = []; // task status
		for ($i = 0; $i<count($donetasks); $i++) {
			$donetask = $donetasks[$i];
			if ($donetask == null) {
				$taskstatus[] = [ 'status' => 'white', 'mark' => '', 'notsubmmited' => true, 'history' => '' ]; // pending
				$header[$i]['not'] = $header[$i]['not'] + 1;
			} else {
				$endmark = $donetask['mark'];
				$history = nl2br($donetask['history'], true);
				if ($endmark == 0) {
					$taskstatus[] = [ 'status' => 'slswrong', 'mark' => '<div class="label">0</div>', 'history' => $history ]; // wrong
					$header[$i]['wrong'] = $header[$i]['wrong'] + 1;			
				} elseif ($endmark == $ranks[$i]['maxrank']) {
					$taskstatus[] = [ 'status' => 'slscorrect', 'mark' => '<div class="label">'.$endmark.'</div>', 'history' => $history ]; // success
					$header[$i]['correct'] = $header[$i]['correct'] + 1;
				} else { 
					$taskstatus[] = [ 'status' => 'slspartially', 'mark' => '<div class="label">'.$endmark.'</div>', 'history' => $history ]; // good
					$header[$i]['partial'] = $header[$i]['partial'] + 1;
				}
			}
		}
		
		// general counts
		if ($assstatus['text'] == 'O') 
			$overdue = $overdue + 1;
		elseif ($assstatus['text'] == 'C')
			$complete = $complete + 1;
		else
			$incomplete = $incomplete + 1;
				
		// values for template
		$data['users'][] = [ 'name' => $user->firstname . ' ' . $user->lastname, 'views' => $viewperuser->ct, 'online' => $online, 'assessment' => $assstatus, 'taskstatus' => $taskstatus ];
	}

	// status overview
	$total = $complete + $incomplete + $overdue; 
	$completepct = round(100 * ($complete / $total));
	$incompletepct = round(100 * ($incomplete / $total));
	$overduepct = round(100 * ($overdue / $total));

	$data['overview'] = [ 'complete' => $complete, 'incomplete' => $incomplete, 'overdue' => $overdue, 
			'completepct' => $completepct, 'incompletepct' => $incompletepct, 'overduepct' => $overduepct ];

	// header
	$sum = $header[0]['not'] + $header[0]['wrong'] + $header[0]['correct'] + $header[0]['partial'];
	foreach ($header as $i => $counter) {
		$header[$i]['notpct'] = round(100 * ($counter['not']/$sum));
		$header[$i]['correctpct'] = round(100 * ($counter['correct']/$sum));
		$header[$i]['partialpct'] = round(100 * ($counter['partial']/$sum));
		$header[$i]['wrongpct'] = round(100 * ($counter['wrong']/$sum));
	}

	$data['header'] = $header;
}

echo $OUTPUT->render_from_template('mod_laejss/lti_report', $data);
