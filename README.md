# Moodle Plugin EjsS Learning Analytics

This Moodle plugin provides teachers with learning analytics tools for courses based on Easy JavaScript Simulation (EjsS).

## Deployment
- [Moodle 402 Deployment](https://iwant2study.org/moodle402/login/index.php)
- [Student Learning Space SLS](https://vle.learning.moe.edu.sg/login)
- [Student Learning Space SLS LTI consumer documentation](https://www.learning.moe.edu.sg/teacher-user-guide/discover/embed-integrated-apps/)

## Video Tutorials
- [YouTube Playlist](https://www.youtube.com/playlist?list=PLYIwRBA8ZhdMO641tySaC_Qu0fgfvHkZg)

## Main Features
- A new resource called EjsS Simulation to include in any course.
- Usage information of EjsS simulations and actions done by students (i.e., properties changes and events).
- Real-time control panel to monitor the learning process of the students using EjsS simulations.

## Installation
1. Install the `plugin.zip` file.
2. Enable the web service advanced feature (Admin > Advanced features).
3. Enable REST protocol (Admin > Server > Web services > Manage protocols).
4. Allow users to create a web service token (Admin > Users > Permissions > Define roles).

## File Descriptions

### Main Files
- **index.php**: Main entry point for the plugin, typically displaying the main page.
- **settings.php**: Contains the settings configuration for the plugin within the Moodle administration interface.
- **lib.php**: Library functions used by the plugin, often utilized across different parts of the plugin.
- **report_course.php**: Generates and handles course-related reports.
- **version.php**: Version information for the plugin, including version number and compatibility details.
- **report_lti_lib.php**: Library functions related to LTI (Learning Tools Interoperability) reports.
- **report_lib.php**: General report-related library functions.
- **report_actions.php**: Manages actions related to reports, such as generating or exporting data.
- **resource_lib.php**: Manages resource-related functionalities for the plugin.
- **externallib.php**: External library functions that might be called via web services or other external integrations.
- **README.md**: Provides an overview and documentation for the plugin, including installation and usage instructions.
- **report_status.php**: Manages the status reporting functionality of the plugin.
- **report_user.php**: Handles user-specific reports.
- **view.php**: Responsible for displaying specific views or pages within the plugin.
- **report_allcourses.php**: Generates reports for all courses.
- **report_search.php**: Manages the search functionality within reports.
- **mod_form.php**: Form definition for the plugin's module settings or configuration.
- **LICENSE**: Licensing information for the plugin, specifying how it can be used and distributed.
- **constants.php**: Defines constants used throughout the plugin for configuration and settings.
- **report_monitor.php**: Monitors and generates reports based on certain criteria or metrics.
- **report_history.php**: Handles historical data and generates related reports.
- **report_lti.php**: Manages LTI-related reports.

### Database Files
- **db/services.php**: Defines web services provided by the plugin.
- **db/access.php**: Manages access permissions and capabilities for the plugin.
- **db/events.php**: Defines events triggered by the plugin.
- **db/log.php**: Manages logging of plugin-related actions and events.
- **db/install.xml**: Database installation instructions, such as table creation and initial data insertion.

### Templates
- **templates/assessment.json.rename21CC.json**: A template for assessment JSON configuration, likely renamed for clarity or versioning.
- **templates/MakeAssessmentJson_21cc.py**: Python script to generate assessment JSON files.
- **templates/assessment.json.template**: Template for assessment configuration in JSON format.
- **templates/lti_report.mustache**: Mustache template for rendering LTI reports.
- **templates/questionLib.js**: JavaScript library for managing questions within the plugin.

### Assets
- **pix/icon.png**: Icon image file used by the plugin, typically displayed in the Moodle interface.

### Classes
- **classes/observer.php**: Observer classes that respond to events triggered within the plugin or Moodle.
- **classes/event/course_module_instance_list_viewed.php**: Defines an event for when a course module instance list is viewed.
- **classes/event/course_module_viewed.php**: Defines an event for when a course module is viewed.

### Language
- **lang/en/laejss.php**: Language strings for the plugin, providing localization and translation for English.

These files together form the structure and functionality of the Moodle plugin, handling various aspects like configuration, reporting, event management, and user interaction.

## Plugin Update
To avoid issues with updates, we recommend purging caches (Admin > Development > Purge caches).

## Acknowledgments
- **2023**: These EjsS are a collection of virtual science labs, digital manipulatives, and captivating games designed to enhance learning experiences through learning analytics capabilities. 
  - [How to add EjsS in the EjsS App to your SLS assignment/lesson](https://iwant2study.org/ospsg/index.php/projects/575-aep-01-18-lw-virtual-lab-learning-analytics-moodle-extension)
  - [How to use the EjsS authoring toolkit to add learning analytics capabilities](https://iwant2study.org/ospsg/index.php/projects/1028-2021-senior-specialist-track-research-fund-sstrf-learning-tools-interoperability-lti-exploratory-learning-environment-ele-data-analytics)
  - This EjsS App is funded by the MOE Senior Specialist Research - Development Fund (SSTRF).
- **2018**: This research is made possible by the eduLab project AEP 01/18 LW Virtual Lab Learning Analytics-Moodle extension, awarded by the Ministry of Education (MOE), Singapore, in collaboration with the National Institute of Education (NIE), Singapore.

## Contact
- [Loo Kang Wee](http://weelookang.blogspot.com/)
- [Félix J. García Clemente](http://webs.um.es/fgarcia/miwiki/doku.php?id=home)

## References
- [2018 Project details](https://iwant2study.org/ospsg/index.php/projects/575-aep-01-18-lw-virtual-lab-learning-analytics-moodle-extension)
- [2021 Project details](https://iwant2study.org/ospsg/index.php/projects/1028-2021-senior-specialist-track-research-fund-sstrf-learning-tools-interoperability-lti-exploratory-learning-environment-ele-data-analytics)
