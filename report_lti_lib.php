<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lib for LTI report
 *
 * @package    mod_laejss
 * @copyright  2022 Felix J. Garcia
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once(__DIR__. '/constants.php');

function mod_laejss_lti_getInteractions($userid, $cmid, $resourcelink) {
    global $DB;

    $interactions = [];
    $params = ['cmid' => $cmid, 'userid' => $userid];
    $sql = "SELECT ud.info
            FROM {". PLUGIN_VIEWS_TABLE_NAME ."} v
            JOIN {". PLUGIN_USERDATA_TABLE_NAME ."} ud ON v.id = ud.viewid
            WHERE v.contextinstanceid = :cmid AND v.userid = :userid";

 	// use resourcelink in sql
	 if (!empty($resourcelink)) {
        $sql .= " AND v.ltiresourcelink = :resourcelink";
        $params['resourcelink'] = $resourcelink;
    }

    $records = $DB->get_recordset_sql($sql, $params);

    foreach ($records as $record) {
        $json = json_decode($record->info);
        if (!empty($json->interactions)) {
            $interactions[] = $json->interactions;
        }
    }
    $records->close(); 

    return $interactions;
}
function mod_laejss_lti_findInteractionsPeriod($interactions, $start, $end) {
	$periods = [];
	for($i = 0; $i < count($interactions); $i++) {
		$started = -1;
		for($j = 0; $j < count($interactions[$i]); $j++) {
			$action = $interactions[$i][$j];
			if ($started == -1) {
				// not $start found
				if (mod_laejss_lti_equals($action, $start)) {
					$started = $j;	// found
				}
			} elseif ($started > -1) {
				// $start found
				if (mod_laejss_lti_equals($action, $end)) {
					// save period
					$periods[] = [ "view" => $i, "start" => $started, "end" => $j ];
				} elseif (mod_laejss_lti_isChangedProperty($action, $start)) {
					$started = -1; // lost
				} 
			}
		}
	}
	return $periods;
}

function mod_laejss_lti_checkState($interactions, $sequence, $history, $username) {	
	// get elements and properties of interest
	$interest = [];
	for($j = 0; $j < count($sequence); $j++) {
		$seqact = $sequence[$j];
		$interest[] = [ "element" => $seqact->{'element'}, "property" => $seqact->{'property'}, "data" => null ];
		if (isset($seqact->{'equals'})) {
			$interest[] = [ "element" => $seqact->{'equals'}, "property" => $seqact->{'property'}, "data" => null ];
		}
	}
	
	// also history 
	if (isset($history))
		$histinterest = [ "element" => $history->{'element'}, "property" => $history->{'property'}, "data" => null ];
	
	// get last data for elements and properties of interest
	for($i = 0; $i < count($interactions); $i++) {			
		$action = $interactions[$i];
		// of interest?
		for($j = 0; $j < count($interest); $j++) {
			if (mod_laejss_lti_equals($action, $interest[$j], false)) {
				$interest[$j]["data"] = $action->{'data'};
			} 
		}
		if (isset($history) and mod_laejss_lti_equals($action, $histinterest, false)) {
			$histinterest["data"] = $action->{'data'};
		}
	}

	// check values with expected
	$checking = array_fill(0, count($sequence), False);
	for($j = 0; $j < count($sequence); $j++) {
		$seqact = $sequence[$j];
		if (isset($seqact->{'equals'})) { // more operators?
			// first
			$first = [ "element" => $seqact->{'element'}, "property" => $seqact->{'property'}, "data" => null ];
			for($i = 0; $i < count($interest); $i++) {
				if (mod_laejss_lti_equals($interest[$i], $first, false))
					$first["data"] = $interest[$i]['data'];
			}
			// second
			$second = [ "element" => $seqact->{'equals'}, "property" => $seqact->{'property'}, "data" => null ];
			for($i = 0; $i < count($interest); $i++) {
				if (mod_laejss_lti_equals($interest[$i], $second, false))
					$second["data"] = $interest[$i]['data'];
			}
			// compare
			//if ($first["data"] == $second["data"])
			//	print_error($first["data"] . " - " . $second["data"] . " - True" );
			//else
			//	print_error($first["data"] . " - " . $second["data"] . " - False" );
			$checking[$j] = (boolean) ($first["data"] == $second["data"]);
		} else {
			for($i = 0; $i < count($interest); $i++) {			
				if (mod_laejss_lti_equals($seqact, $interest[$i]))
					$checking[$j] = True;
			}
		}
	}

	// prepare result
	$result = True;
	for($j = 0; $j < count($checking); $j++) {
		$result = ($result and (boolean) $checking[$j]);
	}
	//print_error(json_encode($result));
	return [ "found" => $result, "history" => $histinterest["data"] ];
	
/*	
	$historydata = 'Not avaiable history';
	$checking = array_fill(0, count($sequence), False);
	for($i = 0; $i < count($interactions); $i++) {			
		$action = $interactions[$i];
		// check sequence
		for($j = 0; $j < count($sequence); $j++) {
			if (mod_laejss_lti_equals($action, $sequence[$j])) {
				$checking[$j] = True;
			} elseif (mod_laejss_lti_isChangedProperty($action, $sequence[$j])) {
				$checking[$j] = False;
			}				
		}
		// check history
		if (mod_laejss_lti_equals($action, $history)) {
			$historydata = $action->{'data'};
		}				
	}
	$result = True;
	for($j = 0; $j < count($sequence); $j++) {
		$result = $result and $sequence[$j];
	}
	return [ "found" => $result, "history" => $historydata ];
*/
}

function mod_laejss_lti_equals($p1, $p2, $withdata = True) {
	$action1 = (object) $p1;
	$action2 = (object) $p2;
	$equals = False;
	if (isset($action1) and isset($action2) and 
	    isset($action1->{'element'}) and isset($action2->{'element'})) {
		$ele1txt = strtolower($action1->{'element'});
		$ele2txt = strtolower($action2->{'element'});
		if (($ele1txt == $ele2txt) or ($ele1txt == 'any') or ($ele2txt == 'any')) {
			// check element property
			if (isset($action1->{'property'}) and isset($action2->{'property'})) {
				$prop1 = strtolower($action1->{'property'});
				$prop2 = strtolower($action2->{'property'});
				if (($prop1 == $prop2) or ($prop2 == 'any') or ($prop2 == 'any')) {
					if (!$withdata) {
						$equals = True;
					} else {
						if (isset($action1->{'data'}) and isset($action2->{'data'})) {
							$data1 = $action1->{'data'};
							$data2 = $action2->{'data'};
							if ($data1 == $data2 or 
							   (is_string($data1) and strtolower($data1) == 'any') or 
							   (is_string($data2) and strtolower($data2) == 'any')) {
								$equals = True;
							}
						} elseif (!isset($action1->{'data'}) and !isset($action2->{'data'})) {
							$equals = True;
						}
					}
				}
			// check action (OnSomething)
			} elseif (isset($action1->{'action'}) and isset($action2->{'action'})) {
					$a1 = strtolower($action1->{'action'});
					$a2 = strtolower($action2->{'action'});
					if (($a1 == $a2) or ($a1 == 'any') or ($a2 == 'any')) {
						$equals = True;
					}
			} 
		}
	}
	return $equals;
}

function mod_laejss_lti_isChangedProperty($p1, $p2) {
	$action1 = (object) $p1;
	$action2 = (object) $p2;
	$changed = False;
	$ele1txt = strtolower($action1->{'element'});
	$ele2txt = strtolower($action2->{'element'});	
	if ($ele1txt == $ele2txt or $ele1txt == "any" or $ele2txt == "any") {
		// check element property
		if (isset($action1->{'property'}) and isset($action2->{'property'})) {
			$prop1 = strtolower($action1->{'property'});
			$prop2 = strtolower($action2->{'property'});
			if ($prop1 == $prop2 or $prop2 == 'any' or $prop2 == 'any') {
				if (isset($action1->{'data'}) and isset($action2->{'data'})) {
					$data1 = $action1->{'data'};
					$data2 = $action2->{'data'};
					if ($data1 != $data2) {
						$changed = True;
					}
				}
			}
		} 
	}
	return $changed;
}
