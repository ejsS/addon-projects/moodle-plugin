<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Constants
 *
 * @package    mod_laejss
 * @copyright  2022 Felix J. Garcia
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

define('PLUGIN_VIEWS_TABLE_NAME', 'laejss_views');
define('PLUGIN_USERDATA_TABLE_NAME', 'laejss_userdata');
define('EJSSIMULATION_TABLE_NAME', 'laejss');

define('COURSE_TABLE_NAME', 'course');
define('COURSE_MODULES_TABLE_NAME', 'course_modules');
define('MODULES_TABLE_NAME', 'modules');
define('USER_TABLE_NAME', 'user');
define('TOKEN_TABLE_NAME', 'external_tokens');
define('EXTERNAL_SERVICE_TABLE_NAME', 'external_services');
define('ENROL_TABLE_NAME', 'enrol');
define('USER_ENROLMENTS_TABLE_NAME', 'user_enrolments');
define('SESSIONS_TABLE_NAME', 'sessions');

define('EXTERNAL_SERVICE_NAME', 'LA EjsS Interactions Report');




